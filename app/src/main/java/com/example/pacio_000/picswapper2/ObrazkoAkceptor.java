package com.example.pacio_000.picswapper2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class ObrazkoAkceptor extends AppCompatActivity {
    private ToucheImageView ob;
    private Uzytkownik uz;
    private String adreso;
    private String wybrany;
    private File glowny;
    private Bitmap bit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obrazko_akceptor);
        Intent i=getIntent();
        uz=(Uzytkownik)i.getSerializableExtra("papa");
        if(uz==null) {
            Button button = (Button) findViewById(R.id.button10);
            button.setTextColor(Color.parseColor("#E50682"));
            // OdbieraniePlikowLokalnie opl=new OdbieraniePlikowLokalnie();
            //  ToucheImageView img= (ToucheImageView) findViewById(R.id.image);
            ob = (ToucheImageView) findViewById(R.id.image);
            ob.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    ob.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    Drawable drawable = ob.getDrawable();
                    Rect rectDrawable = drawable.getBounds();
                    float leftOffset = (ob.getMeasuredWidth() - rectDrawable.width()) / 2f;
                    float topOffset = (ob.getMeasuredHeight() - rectDrawable.height()) / 2f;

                    Matrix matix = ob.getImageMatrix();
                    matix.postTranslate(leftOffset, topOffset);
                    ob.setImageMatrix(matix);
                    ob.invalidate();

                }
            });
            Intent intent = getIntent();
            String akcja = intent.getAction();
            String typ = intent.getType();


            if (Intent.ACTION_SEND.equals(akcja) && typ != null) {
                if (typ.startsWith("image/")) {
                    try {
                        imagor(intent);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            Intent najpierw=new Intent(getApplicationContext(),LogowanieACK.class);
            Log.d("adred13233",": adres uri przed wyslaniem do logowania"+adreso);
            Log.d("adred13233",": stary sposob: "+i.getParcelableExtra(Intent.EXTRA_STREAM));
            najpierw.putExtra("uri",adreso);
            startActivity(najpierw);
        }else{
            Intent dawaj=getIntent();
            String adres= (String) dawaj.getSerializableExtra("uri");
            adreso=adres;
            Log.d("adred13233",": zWROTNY: "+adres);
           // Bitmap bit= null;
            try {
                bit = MediaStore.Images.Media.getBitmap(this.getContentResolver(),Uri.parse(adres));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Button button = (Button) findViewById(R.id.button10);
            button.setTextColor(Color.parseColor("#E50682"));
            ob = (ToucheImageView) findViewById(R.id.image);
            ob.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    ob.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    Drawable drawable = ob.getDrawable();
                    Rect rectDrawable = drawable.getBounds();
                    float leftOffset = (ob.getMeasuredWidth() - rectDrawable.width()) / 2f;
                    float topOffset = (ob.getMeasuredHeight() - rectDrawable.height()) / 2f;

                    Matrix matix = ob.getImageMatrix();
                    matix.postTranslate(leftOffset, topOffset);
                    ob.setImageMatrix(matix);
                    ob.invalidate();

                }
            });
            ob.setImageBitmap(bit);
            Uzytkownik uz= (Uzytkownik) dawaj.getSerializableExtra("papa");
            final File pliko=new File(getApplicationContext().getFilesDir().getAbsoluteFile().getPath());
            OdbieraniePlikowLokalnie opl=new OdbieraniePlikowLokalnie(pliko,uz);
            opl.execute();
            AlertDialog.Builder b=new AlertDialog.Builder(ObrazkoAkceptor.this);
            b.setIcon(R.drawable.ic_folder2);
            b.setTitle("Wybierz lokalizacje:");
            final ArrayAdapter<String> foldery=new ArrayAdapter<String>(ObrazkoAkceptor.this, R.layout.select_dialog_singlechoice_material);
            try {
                boolean mozna=opl.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            for(int n=0;n<PlikiLista.foldery2.size();n++){
                File plik=new File(PlikiLista.foldery2.get(n).getAdresAndroid());
                foldery.add(plik.getName());
            }
            b.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            b.setAdapter(foldery, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    wybrany=foldery.getItem(which);
                    Log.d("Checkero","U zrodla: "+wybrany);
                    for(int n=0;n<PlikiLista.foldery2.size();n++){
                        Log.d("folderdeep",": "+n+" : "+PlikiLista.foldery2.get(n).getAdresAndroid());
                    }
                    for(int n=0;n<PlikiLista.foldery2.size();n++){

                        File tmpPliko=new File(PlikiLista.foldery2.get(n).getAdresAndroid());
                        Log.d("Checkero","tmpPliko: "+tmpPliko.getName()+" wybrany: "+wybrany);
                        if(tmpPliko.getName().equals(wybrany)){
                            glowny=new File(PlikiLista.foldery2.get(n).getAdresAndroid());

                        }
                    }
                    try {
                        glowny.createNewFile();
                        Log.d("Checkero","adreso: "+adreso);
                        File piczko=new File(adreso);
                        int index=piczko.getName().indexOf(".");
                        String nazwa=piczko.getName().substring(0,index);
                        nazwa+=".png";
                        Log.d("Checkero","piczko po zabawach: "+nazwa);
                        FileOutputStream fos =new FileOutputStream(glowny.getPath()+File.separator+nazwa);

                        Log.d("Checkero","Zapisuje jako: "+glowny.getPath()+File.separator);
                        bit.compress(Bitmap.CompressFormat.PNG,100,fos);
                        fos.close();
                        String tmo=glowny.getPath();
                        int index2=tmo.indexOf("files");
                        String adr=tmo.substring(index2+1,tmo.length());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            b.show();



        }
    }
    public void imagor(Intent i) throws IOException {
        Uri adres=i.getParcelableExtra(Intent.EXTRA_STREAM);
        if(adres!=null){
            adreso=adres.toString();
            Log.d("imgAlkceptor","URI: "+adres);
            Bitmap bit= MediaStore.Images.Media.getBitmap(this.getContentResolver(),adres);
            ob.setImageBitmap(bit);
        }

    }
}
