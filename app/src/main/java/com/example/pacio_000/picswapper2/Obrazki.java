package com.example.pacio_000.picswapper2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;

public class Obrazki extends AppCompatActivity {
    String adres=null;

    public Obrazki(String adres){
        this.adres=adres;
        Intent intent=new Intent(Etykiety.cont,Obrazki.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Etykiety.cont.startActivity(intent);
    }
    public Obrazki(){

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obrazki);
        if(adres==null){
          adres=MyAdapter.adreso;
        }

        Button przycisk= (Button) findViewById(R.id.button2);
        przycisk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });
        Log.d("adreso","Adres: "+adres);
        File plik =new File(adres);
        final Bitmap bit= BitmapFactory.decodeFile(plik.getAbsolutePath());
        Context co=this;
        final ToucheImageView ob= (ToucheImageView) findViewById(R.id.image);
        ob.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener(){
            public void onGlobalLayout(){
                ob.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                Drawable drawable=ob.getDrawable();
                Rect rectDrawable=drawable.getBounds();
                float leftOffset=(ob.getMeasuredWidth()-rectDrawable.width())/2f;
                float topOffset=(ob.getMeasuredHeight()-rectDrawable.height())/2f;

                Matrix matix=ob.getImageMatrix();
                matix.postTranslate(leftOffset,topOffset);
                ob.setImageMatrix(matix);
                ob.invalidate();

            }
        });
        ob.setImageBitmap(bit);
    }
}
