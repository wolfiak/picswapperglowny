package com.example.pacio_000.picswapper2;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.util.concurrent.ExecutionException;

public class Splash extends AppCompatActivity {
  public static int SPLASH_DISPLAY_LENGTH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final ImageView im2=(ImageView) findViewById(R.id.imageView2);
        Intent i =getIntent();
         final Uzytkownik uz= (Uzytkownik) i.getSerializableExtra("papa");
        final File pliko=new File(getApplicationContext().getFilesDir().getAbsoluteFile().getPath());
        im2.setImageResource(R.mipmap.ic_launcher);
        Etykiety.cont=getApplicationContext();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
               // OdbieraniePlikowLokalnie lol=new OdbieraniePlikowLokalnie(pliko,uz);
               // lol.execute();
                OdbieraniePlikowLokalnie lol=new OdbieraniePlikowLokalnie(pliko,uz);
                lol.execute();
                ChackerDoSplash chack=new ChackerDoSplash(pliko,uz,lol);
                chack.execute();
                try {
                    Log.d("czekanieSplash","Czekam na zwrot");
                    boolean b=chack.get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
               // Kolejny.runPRoces(uz,lol);
                Intent main=new Intent(Splash.this,Kolejny.class);
                Intent i =getIntent();
                final Uzytkownik uz= (Uzytkownik) i.getSerializableExtra("papa");
                main.putExtra("papa",uz);

                Splash.this.startActivity(main);
                //ChackerDoSplash lolo=new ChackerDoSplash();
               // lolo.execute();

                Splash.this.finish();

            }
        },SPLASH_DISPLAY_LENGTH);
    }
}
